var dssv = [];
var DSSV = "DSSV";

var dssvJson = localStorage.getItem("DSSV");
if (dssvJson != null){
    var svArr = JSON.parse(dssvJson);
    dssv = svArr.map(function(item){
        return new SinhVien(item.maSV, 
            item.tenSV, 
            item.email, 
            item.matKhau, 
            item.diemToan,
            item.diemLy, 
            item.diemHoa)
    });
    renderDSSV(dssv);
}
function themSinhVien(){
    var sv = layThongTinTuForm();

    // kiểm tra trùng
    var isValid = true;

    isValid = kiemTraTrung(sv.maSV, dssv) & kiemTraDoDai(sv.maSV, "spanMaSV", 4, 8) & kiemTraSo(sv.maSV,"spanMaSV") & kiemTraEmail(sv.email, "spanEmailSV") & kiemTraDoDai(sv.matKhau, "spanMatKhau", 8, 16) & kiemTraSo(sv.diemToan, "spanToan") & kiemTraSo(sv.diemLy, "spanLy") & kiemTraSo(sv.diemHoa, "spanHoa");
    if(isValid){
        dssv.push(sv);
        var dssvJson = JSON.stringify(dssv);
        localStorage.setItem(DSSV, dssvJson);
        renderDSSV(dssv);
    }
}


function xoaSinhVien(idSV){
    var viTri = timKiemViTri(idSV, dssv);
    if(viTri != 1){
        dssv.splice(viTri, 1);
        renderDSSV(dssv);
    }
}

function suaSinhVien(idSV){
    var viTri = timKiemViTri(idSV, dssv);
    if(viTri == -1){
        return;
    }
    var sv = dssv[viTri];
    showThongTinLenForm(sv);
}

function capNhatSinhVien(){
    var sv = layThongTinTuForm();
    var viTri = timKiemViTri(sv.maSV, dssv);
    if(viTri != -1){
        dssv[viTri] = sv;
        renderDSSV(dssv);
    }
}




