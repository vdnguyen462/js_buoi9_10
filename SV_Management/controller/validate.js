function kiemTraTrung(idSV, svArr){
    var index = svArr.findIndex(function(item){
        return idSV == item.maSV;
    });
    if(index == -1){
        document.getElementById("spanMaSV").innerText = "";
        return true;
    }
    else{
        document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại";
        return false;
    }
}

function kiemTraDoDai(value, idErr, min, max){
    var length = value.length;
    if(length < min || length > max){
        document.getElementById(idErr).innerText = `Độ dài phải từ ${min} đến ${max} ký tự`;
        return false;
    }
    else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

function kiemTraSo(value, idErr){
    var reg = /^\d+$/;
    var isNumber = reg.test(value);
    if(isNumber){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Trường này phải là số";
        return false;
    }
}

function kiemTraEmail(value, idErr){
    var reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = reg.test(value);
    if(isEmail){
        document.getElementById(idErr).innerText = "";
        return true;
    }
    else{
        document.getElementById(idErr).innerText = "Trường này phải là email";
        return false;
    }
}