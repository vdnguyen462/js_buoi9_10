function layThongTinTuForm(){
    const _maSV = document.getElementById("txtMaSV").value;
    const _tenSV = document.getElementById("txtTenSV").value;
    const _email = document.getElementById("txtEmail").value;
    const _matKhau = document.getElementById("txtPass").value;
    const _diemToan = document.getElementById("txtDiemToan").value*1;
    const _diemLy = document.getElementById("txtDiemLy").value*1;
    const _diemHoa = document.getElementById("txtDiemHoa").value*1;

    var sv = new SinhVien(_maSV, _tenSV, _email, _matKhau, _diemToan, _diemLy, _diemHoa)
    return sv;
}

function renderDSSV(svArr){
    var contentHTML = "";
    for (var index = 0; index < svArr.length; index++){
        var item = svArr[index];
        var contentTr = `<tr>
                            <td>${item.maSV}</td>
                            <td>${item.tenSV}</td>
                            <td>${item.email}</td>
                            <td>${item.tinhDTB()}</td>
                            <td><button onclick="xoaSinhVien('${item.maSV}')" class="btn btn-danger"> Xoá</button>
                            <button onclick="suaSinhVien('${item.maSV}')" class="btn btn-warning">Sửa</button></td>
                        </tr>`
        contentHTML+=contentTr;
    }
    document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr){
    var viTri = -1;
    for(var index=0; index < arr.length; index++){
        var sv = arr[index];
        if(sv.maSV == id){
            viTri = index;
            break;
        }
    }
    return viTri;
}

function showThongTinLenForm(sv){
    document.getElementById("txtMaSV").value = sv.maSV;
    document.getElementById("txtTenSV").value = sv.tenSV;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
}